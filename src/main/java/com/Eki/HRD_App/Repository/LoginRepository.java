package com.Eki.HRD_App.Repository;

import com.Eki.HRD_App.entity.Login;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LoginRepository extends JpaRepository<Login, Integer> {

    Optional<Login> findByEmail(String email);

    Optional<Login> findByEmailAndPassword(String email, String password);

    Optional<Login> findByToken(String token);

    Optional<Login> findByHrdId(Integer id);

}
