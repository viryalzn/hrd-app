package com.Eki.HRD_App.Repository;

import com.Eki.HRD_App.entity.HRD;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HRDRepository extends JpaRepository<HRD, Integer> {

}
