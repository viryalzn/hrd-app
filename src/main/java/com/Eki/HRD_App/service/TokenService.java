package com.Eki.HRD_App.service;

import com.Eki.HRD_App.Repository.LoginRepository;
import com.Eki.HRD_App.entity.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TokenService {

    @Autowired
    LoginRepository loginRepository;

    public boolean getToken(String token) {
        Optional<Login> getToken = loginRepository.findByToken(token);
        boolean validate = getToken.isPresent() ? true : false;
        return validate;
    }

}
