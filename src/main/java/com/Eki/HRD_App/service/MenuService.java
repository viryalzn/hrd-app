package com.Eki.HRD_App.service;

import com.Eki.HRD_App.Repository.MenuRepository;
import com.Eki.HRD_App.entity.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuService {

    @Autowired
    MenuRepository menuRepository;

    public List<Menu> getAllMenu() {
        List<Menu> response = menuRepository.findAll();
        return response;
    }

}
