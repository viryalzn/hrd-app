package com.Eki.HRD_App.service;

import com.Eki.HRD_App.Repository.HRDRepository;
import com.Eki.HRD_App.Repository.LoginRepository;
import com.Eki.HRD_App.dto.HRDRequest;
import com.Eki.HRD_App.dto.HRDResponse;
import com.Eki.HRD_App.entity.HRD;
import com.Eki.HRD_App.entity.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class HRDRegisterService {

    @Autowired
    private LoginRepository loginRepository;
    @Autowired
    private HRDRepository hrdRepository;

    public HRDResponse registerHRD(HRDRequest request) throws Exception{
        HRDResponse response = new HRDResponse();
        Login login = new Login();
        HRD hrd = new HRD();
        Random random = new Random();

        String email = request.getEmail();
        if (loginRepository.findByEmail(email).isPresent()){
            throw new Exception("Email Sudah Ada!!!");
        }

        hrd.setEmail(email);
        hrd.setNama(request.getNama());
        hrd.setPhone(request.getPhone());
        hrd.setAlamat(request.getAlamat());
        hrdRepository.save(hrd);

        login.setEmail(request.getEmail());
        login.setPassword(request.getPassword());
        login.setHrd(hrd);
        loginRepository.save(login);

        response.setEmail(hrd.getEmail());
        response.setNama(hrd.getNama());
        response.setPhone(hrd.getPhone());
        response.setAlamat(hrd.getAlamat());


        if (request.getNama() != null && request.getAlamat() != null && request.getEmail() !=null && request.getPhone() != null){
            hrd.setNip("HR" + String.format("%06d", random.nextInt(999999)));
            hrdRepository.save(hrd);

            response.setNip(hrd.getNip());
        }else {
            throw new Exception();
        }
        return response;
    }

}
