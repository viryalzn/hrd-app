package com.Eki.HRD_App.service;

import com.Eki.HRD_App.Repository.DepartmentRepository;
import com.Eki.HRD_App.Repository.EmployeeRepository;
import com.Eki.HRD_App.Repository.LoginRepository;
import com.Eki.HRD_App.dto.EmployeeRequest;
import com.Eki.HRD_App.dto.EmployeeResponse;
import com.Eki.HRD_App.entity.Department;
import com.Eki.HRD_App.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    TokenService tokenService;

    @Autowired
    LoginRepository loginRepository;

    public EmployeeResponse addEmployee(String token, EmployeeRequest request) throws Exception {
        EmployeeResponse response = new EmployeeResponse();
        Optional<Department> optionalDepartment = departmentRepository.findById(request.getDepartmentId());

        if (tokenService.getToken(token)) {
            Employee employee = new Employee();
            Random random = new Random();

            String email = request.getEmail();
            if (employeeRepository.findByEmail(email).isPresent()) {
                throw new Exception("Email Employee Sudah Ada!!!");
            }

            employee.setName(request.getName());
            employee.setEmail(request.getEmail());
            employee.setPhone(request.getPhone());
            employee.setAlamat(request.getAlamat());
            employee.setDepartment(optionalDepartment.get());
            employeeRepository.save(employee);

            response.setName(employee.getName());
            response.setEmail(employee.getEmail());
            response.setPhone(employee.getPhone());
            response.setAlamat(employee.getAlamat());
            response.setDepartment(optionalDepartment.get());

            if (request.getName() != null && request.getEmail() != null && request.getPhone() != null && request.getAlamat() != null) {
                employee.setNip("EM" + String.format("%06d", random.nextInt(999999)));
                employeeRepository.save(employee);

                response.setNip(employee.getNip());
            }
        } else {
            throw new Exception();
        }
        return response;
        }

//    public List<Employee> viewAllEmployee(String token) throws RuntimeException {
//        List<Employee> employeeList = null;
//
//        if (tokenService.getToken(token)){
//            employeeList = employeeRepository.findAll();
//        } else {
//            throw new RuntimeException();
//        }
//
//        return employeeList;
//    }

    public List<Employee> viewAllEmployee() throws RuntimeException {
        List<Employee> employeeList = null;

        employeeList = employeeRepository.findAll();

        return employeeList;
    }

    public EmployeeResponse updateEmployee (String token, Integer id, EmployeeRequest request) throws Exception {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        EmployeeResponse response = new EmployeeResponse();

        Optional<Employee> emailExist = employeeRepository.findByEmail(request.getEmail());
        Employee employee = new Employee();

        if (emailExist.isPresent()){
            throw new Exception("Email Employee Sudah Digunakan");
        }else if (tokenService.getToken(token)){

            if (optionalEmployee.isPresent()){
                employee.setId(id);
                employee.setName(request.getName());
                employee.setEmail(request.getEmail());
                employee.setPhone(request.getPhone());
                employee.setAlamat(request.getAlamat());
                employee.setNip(optionalEmployee.get().getNip());
                employee.setDepartment(optionalEmployee.get().getDepartment());
                employeeRepository.save(employee);


                response.setName(employee.getName());
                response.setEmail(employee.getEmail());
                response.setPhone(employee.getPhone());
                response.setAlamat(employee.getAlamat());
                response.setNip(employee.getNip());
                response.setDepartment(employee.getDepartment());
            }else {
                throw new Exception("Employee Tidak Ditemukan");
            }
        }else {
            throw new Exception();
        }
        return response;
    }

    public String deleteEmployeeById(String token, Integer id) throws Exception {
        String response = "";
        Optional<Employee> employee = null;

        if (tokenService.getToken(token)) {
            try {
                employee = employeeRepository.findById(id);

                employeeRepository.deleteById(id);

                response = "Karyawan berhasil dihapus";
            } catch (Exception e) {
                throw new Exception();
            }
        } else {
            throw new RuntimeException();
        }

        return response;
    }

}

