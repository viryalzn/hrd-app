package com.Eki.HRD_App.service;

import com.Eki.HRD_App.Repository.CutiEmployeeRepository;
import com.Eki.HRD_App.Repository.LoginRepository;
import com.Eki.HRD_App.dto.CutiEmployeeRequest;
import com.Eki.HRD_App.dto.CutiEmployeeResponse;
import com.Eki.HRD_App.entity.CutiEmployee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class CutiEmployeeService {

    @Autowired
    CutiEmployeeRepository cutiEmployeeRepository;

    @Autowired
    TokenService tokenService;

    @Autowired
    LoginRepository loginRepository;


    public CutiEmployeeResponse addCuti(String token, CutiEmployeeRequest request) throws Exception {
        CutiEmployeeResponse response = new CutiEmployeeResponse();

        if (tokenService.getToken(token)) {
            CutiEmployee cutiEmployee = new CutiEmployee();
            Random random = new Random();

            cutiEmployee.setNamaKaryawan(request.getNamaKaryawan());
            cutiEmployee.setKeteranganCuti(request.getKeteranganCuti());
            cutiEmployee.setMulaiCuti(request.getMulaiCuti());
            cutiEmployee.setAkhirCuti(request.getAkhirCuti());
            cutiEmployeeRepository.save(cutiEmployee);

            response.setNamaKaryawan(cutiEmployee.getNamaKaryawan());
            response.setKeteranganCuti(cutiEmployee.getKeteranganCuti());
            response.setMulaiCuti(cutiEmployee.getMulaiCuti());
            response.setAkhirCuti(request.getAkhirCuti());


        } else {
            throw new Exception("login dulu cok");
        }
        return response;
    }

    public List<CutiEmployee> viewAllCutiEmployee() throws RuntimeException {
        List<CutiEmployee> cutiEmployeeList = null;

        cutiEmployeeList = cutiEmployeeRepository.findAll();

        return cutiEmployeeList;
    }

}
