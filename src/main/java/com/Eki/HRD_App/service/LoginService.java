package com.Eki.HRD_App.service;

import com.Eki.HRD_App.Repository.HRDRepository;
import com.Eki.HRD_App.Repository.LoginRepository;
import com.Eki.HRD_App.dto.LoginRequest;
import com.Eki.HRD_App.dto.LoginResponse;
import com.Eki.HRD_App.entity.Login;
import com.Eki.HRD_App.util.JwtToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginService {
    @Autowired
    HRDRepository hrdRepository;

    @Autowired
    LoginRepository loginRepository;


    public LoginResponse login (LoginRequest request) throws Exception{
        Optional<Login> login = loginRepository.findByEmailAndPassword(request.getEmail(), request.getPassword());

        Login l = new Login();
        if (login.isPresent()){
            l.setEmail(request.getEmail());
            l.setPassword(request.getPassword());
            l.setToken(JwtToken.getToken(request));
            loginRepository.save(l);
        }else {
            throw new Exception("User not found");
        }
        LoginResponse response = new LoginResponse();
        response.setToken(l.getToken());
        return response;
    }



}
