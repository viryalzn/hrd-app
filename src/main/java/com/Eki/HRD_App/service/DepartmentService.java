package com.Eki.HRD_App.service;

import com.Eki.HRD_App.Repository.DepartmentRepository;
import com.Eki.HRD_App.dto.DepartmentRequest;
import com.Eki.HRD_App.dto.DepartmentResponse;
import com.Eki.HRD_App.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class DepartmentService {

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    TokenService tokenService;

    public DepartmentResponse addDepartment(String token, DepartmentRequest request) throws Exception {
        DepartmentResponse response = new DepartmentResponse();

        if (tokenService.getToken(token)) {
            Department department = new Department();
            Random random = new Random();

            String departmentName = request.getDepartmentName();
            if (departmentRepository.findByDepartmentName(departmentName).isPresent()) {
                throw new Exception("Nama Department sudah ada");
            } else {
                department.setDepartmentName(request.getDepartmentName());
                departmentRepository.save(department);

                response.setId(department.getId());
                response.setDepartmentName(department.getDepartmentName());
            }


        }
        return response;
    }
}
