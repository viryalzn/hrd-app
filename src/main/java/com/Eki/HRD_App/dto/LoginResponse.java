package com.Eki.HRD_App.dto;

import lombok.Data;

@Data
public class LoginResponse {

    private String token;

}
