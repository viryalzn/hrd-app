package com.Eki.HRD_App.dto;

import lombok.Data;

@Data
public class HRDResponse {

    private String nip;
    private String nama;
    private String email;
    private String phone;
    private String alamat;
}
