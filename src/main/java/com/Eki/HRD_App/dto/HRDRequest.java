package com.Eki.HRD_App.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class HRDRequest {

    @NotBlank(message = "Nama Wajib diisi!!!!")
    private String nama;
    @NotBlank(message = "Email Wajib diisi!!!!")
    private String email;
    @NotBlank(message = "Nomor HP Wajib diisi!!!!")
    private String phone;
    @NotBlank(message = "Alamat Wajib diisi!!!!")
    private String alamat;
    @NotBlank(message = "Password Wajib diisi!!!!")
    private String password;
}
