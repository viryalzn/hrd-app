package com.Eki.HRD_App.dto;

import lombok.Data;

@Data
public class EmployeeRequest {
    private String name;
    private String email;
    private String alamat;
    private String phone;

    private Integer departmentId;
}
