package com.Eki.HRD_App.dto;

import lombok.Data;

@Data
public class DepartmentResponse {

    private  Integer id;
    private String departmentName;

}
