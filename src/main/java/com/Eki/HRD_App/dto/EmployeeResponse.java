package com.Eki.HRD_App.dto;

import com.Eki.HRD_App.entity.Department;
import lombok.Data;

@Data
public class EmployeeResponse {
    private String nip;
    private String name;
    private String email;
    private String alamat;
    private String phone;

    private Department department;
}
