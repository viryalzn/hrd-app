package com.Eki.HRD_App.dto;

import lombok.Data;

import java.util.Date;

@Data
public class CutiEmployeeResponse {

    private String namaKaryawan;
    private String keteranganCuti;
    private Date mulaiCuti;
    private Date akhirCuti;

}
