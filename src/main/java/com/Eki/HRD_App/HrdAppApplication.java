package com.Eki.HRD_App;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HrdAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(HrdAppApplication.class, args);
	}

}
