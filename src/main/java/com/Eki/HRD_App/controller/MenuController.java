package com.Eki.HRD_App.controller;

import com.Eki.HRD_App.entity.Menu;
import com.Eki.HRD_App.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    MenuService menuService;

    @GetMapping
    public ResponseEntity<List<Menu>> getAllMenu(){
        List<Menu> menuList = menuService.getAllMenu();
        return ResponseEntity.ok(menuList);
    }

}
