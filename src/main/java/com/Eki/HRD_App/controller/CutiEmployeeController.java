package com.Eki.HRD_App.controller;

import com.Eki.HRD_App.dto.CutiEmployeeRequest;
import com.Eki.HRD_App.dto.CutiEmployeeResponse;
import com.Eki.HRD_App.entity.CutiEmployee;
import com.Eki.HRD_App.service.CutiEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cutiemployee")
public class CutiEmployeeController {

    @Autowired
    CutiEmployeeService cutiEmployeeService;

    @PostMapping
    public ResponseEntity<CutiEmployeeResponse> addCutiEmployee(@RequestHeader("token") String token, @RequestBody CutiEmployeeRequest request) throws Exception{
        CutiEmployeeResponse response = cutiEmployeeService.addCuti(token, request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("view")
    public ResponseEntity<List<CutiEmployee>> viewAllCutiEmployee(){
        List<CutiEmployee> response = cutiEmployeeService.viewAllCutiEmployee();
        return ResponseEntity.ok(response);
    }
}
