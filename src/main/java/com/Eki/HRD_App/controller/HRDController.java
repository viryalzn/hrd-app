package com.Eki.HRD_App.controller;

import com.Eki.HRD_App.dto.HRDRequest;
import com.Eki.HRD_App.dto.HRDResponse;
import com.Eki.HRD_App.service.HRDRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hrdregister")
public class HRDController {

    @Autowired
    HRDRegisterService hrdService;

    @PostMapping
    public ResponseEntity<HRDResponse> registerHRD(@RequestBody HRDRequest request) throws Exception{
        HRDResponse response = hrdService.registerHRD(request);
        return ResponseEntity.ok(response);
    }

}
