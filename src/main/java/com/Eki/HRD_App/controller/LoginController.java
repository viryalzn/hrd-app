package com.Eki.HRD_App.controller;

import com.Eki.HRD_App.dto.LoginRequest;
import com.Eki.HRD_App.dto.LoginResponse;
import com.Eki.HRD_App.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping
    public ResponseEntity<LoginResponse> login (@RequestBody LoginRequest request)throws Exception{
        LoginResponse response = loginService.login(request);
        return ResponseEntity.ok(response);
    }
}
