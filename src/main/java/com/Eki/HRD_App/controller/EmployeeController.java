package com.Eki.HRD_App.controller;

import com.Eki.HRD_App.dto.EmployeeRequest;
import com.Eki.HRD_App.dto.EmployeeResponse;
import com.Eki.HRD_App.entity.Employee;
import com.Eki.HRD_App.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @PostMapping
    public ResponseEntity<EmployeeResponse> addEmployee(@RequestHeader("token") String token, @RequestBody EmployeeRequest request) throws Exception{
        EmployeeResponse response = employeeService.addEmployee(token, request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/view")
    public ResponseEntity<List<Employee>> viewAllEmployee(){
        List<Employee> response = employeeService.viewAllEmployee();
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EmployeeResponse> updateEmployee(@RequestHeader("token") String token, @PathVariable("id") Integer id, @RequestBody EmployeeRequest request)
            throws Exception{
        EmployeeResponse response = employeeService.updateEmployee(token, id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> DeleteEmployeeById(@RequestHeader("token") String token, @PathVariable("id") Integer id) throws Exception{
        String response = employeeService.deleteEmployeeById(token, id);
        return ResponseEntity.ok(response);
    }

}
