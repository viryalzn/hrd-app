package com.Eki.HRD_App.controller;

import com.Eki.HRD_App.dto.DepartmentRequest;
import com.Eki.HRD_App.dto.DepartmentResponse;
import com.Eki.HRD_App.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    DepartmentService departmentService;

    @PostMapping
    public ResponseEntity<DepartmentResponse> addDepartment(@RequestHeader("token") String token, @RequestBody DepartmentRequest request) throws Exception{
        DepartmentResponse response = departmentService.addDepartment(token, request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

}
