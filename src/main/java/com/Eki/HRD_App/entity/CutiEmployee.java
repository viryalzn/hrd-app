package com.Eki.HRD_App.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class CutiEmployee {

    @Id
    @GeneratedValue
    private Integer id;
    private String namaKaryawan;
    private String keteranganCuti;
    private Date mulaiCuti;
    private Date akhirCuti;
}
