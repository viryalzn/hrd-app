package com.Eki.HRD_App.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nip;
    private String name;
    private String email;
    private String alamat;
    private String phone;


    @ManyToOne
    private Department department;

}
