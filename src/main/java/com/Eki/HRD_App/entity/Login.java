package com.Eki.HRD_App.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Login {

    @Id
    private String email;
    private String password;

    @Column (length = 500)
    private String token;

    @OneToOne
    private HRD hrd;
}
